# Arch Linux Auto Installer

Note: All scripts are for MBR/UEFI. You might need to change something in the files if you live in different country.

In this repository you will find packages-scripts for the base install of Arch Linux and the Gnome, KDE, Cinnamon and Xfce desktop environments. More to come for Windows Managers soon.
Modify the packages to your liking, make the script executable with chmod +x scriptname and then run with ./scriptname.

If your PC or laptop is running uefi, you can check out in the "uefi" folder.
Be sure to connect your network first before install git.

Connect your wifi:

```shell
ip -c a # find your interface, such as wlan0 wlp3s0 wlp4s0 or whatever you find

iwctl
station wlan0 scan 
station wlan0 get-networks
station wlan0 connect <your wifi name>
exit
```

Install:

```shell
pacman -Sy git
git clone https://gitlab.com/voxie12/arch-linux-magic.git
cd arch-linux-magic
chmod +x scriptname.sh # if already done
./scriptname.sh # whatever you choose
```
