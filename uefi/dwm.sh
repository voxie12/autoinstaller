# == SIMPLE SUCKLESS INSTALLATION == #
#part1
reflector --verbose -c GB -l 5 --sort rate --save /etc/pacman.d/mirrorlist
pacman -S --noconfirm archlinux-keyring
timedatectl set-ntp true
lsblk
echo "Enter the Drive: "
read drive
fdisk $drive
lsblk
echo "Enter the Linux Partition: "
read partition
mkfs.ext4 $partition
mount $partition /mnt
pacstrap /mnt base linux linux-firmware intel-ucode
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' simple-suckless.sh > /mnt/simple-suckless2.sh
chmod +x /mnt/simple-suckless2.sh
arch-chroot /mnt ./simple-suckless2.sh
exit

#part2
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >/etc/locale.conf
echo "KEYMAP=uk" > /etc/vconsole.conf
echo "Enter the Hostname: "
read hostname
echo $hostname > /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts
passwd
pacman --noconfirm -S grub 
lsblk
echo "Enter grub drive: "
read gdrive
grub-install --target=i386-pc $gdrive
grub-mkconfig -o /boot/grub/grub.cfg

pacman -S --noconfirm xorg xorg-xinit firefox git networkmanager reflector rsync dmenu base-devel
systemctl enable NetworkManager
echo "%wheel ALL=(ALL) NOPASSWD: ALL " >> /etc/sudoers
echo "Enter Username: "
read username 
useradd -mG wheel $username
passwd $username

ss3_path=/home/$username/simple-suckless3.sh
sed '1,/^#part3$/d' simple-suckless2.sh > $ss3_path
chown $username:$username $ss3_path
chmod +x $ss3_path
su -c $ss3_path -s /bin/sh $username
exit

#part3
cd $HOME
repos=( "dwm" "st" "slstatus" )
for repo in ${repos[@]}
do
    git clone git://git.suckless.org/$repo
    cd $repo;sudo make clean install;cd ..
done
echo "exec dwm" >> ~/.xinitrc
cat > ~/.xinitrc << EOF
slstatus &
exec dwm
EOF
echo "startx" >> ~/.bash_profile
EOF
printf "\e[1;32mDone! you can now reboot.\e[0m\n"
exit
