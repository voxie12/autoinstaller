# == MY ARCH LINUX AUTO INSTALLER == #
#part1
echo "Welcome to Arch Linux Magic Script"
setfont ter-i12n
reflector -l 10 -c GB --verbose --sort rate --save /etc/pacman.d/mirrorlist
sed -i 's/^#Para/Para/' /etc/pacman.conf
pacman --noconfirm -Sy archlinux-keyring
loadkeys us
timedatectl set-ntp true
lsblk
echo "Enter the drive: "
read drive
fdisk $drive 
lsblk
echo "Enter the linux partition: "
read partition
mkfs.ext4 $partition 
mount $partition /mnt 
pacstrap /mnt base base-devel linux linux-firmware intel-ucode
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
genfstab -U /mnt >> /mnt/etc/fstab
sed '1,/^#part2$/d' arch_install.sh > /mnt/arch_install2.sh
chmod +x /mnt/arch_install2.sh
arch-chroot /mnt ./arch_install2.sh
exit 

#part2
ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
echo "en_GB.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >> /etc/locale.conf
echo "KEYMAP=uk" >> /etc/vconsole.conf
echo "FONT=ter-i12n" >> /etc/vconsole.conf
echo "Hostname: "
read hostname
echo $hostname >> /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       $hostname.localdomain $hostname" >> /etc/hosts
echo root:jay | chpasswd
pacman --noconfirm -S grub 
lsblk
echo "Enter grub drive: "
read gdrive
grub-install --target=i386-pc $gdrive
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
sed -i 's/^#Para/Para/' /etc/pacman.conf
sed -i 's/^#Col/Col/' /etc/pacman.conf

pacman -S --noconfirm xorg xorg-xinit zsh sxiv xwallpaper starship \
    networkmanager git dash udiskie qbittorrent youtube-dl pacman-contrib \
    ranger neofetch alacritty opendoas expac exa bat dunst unclutter sxhkd \
    openssh yadm neovim terminus-font wget mpv noto-fonts noto-fonts-cjk noto-fonts-emoji \
    telegram-desktop bitwarden-cli reflector rsync pcmanfm linux-headers speedtest-cli

systemctl enable NetworkManager
systemctl enable fstrim.timer
systemctl enable sshd
ln -sfT dash /usr/bin/sh
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "Enter Username: "
read username
useradd -mG wheel -s /bin/zsh $username
passwd $username
echo $username:jay | chpasswd
echo "permit nopass $username" >> /etc/doas.conf
echo "vm.swappiness=10" >> /etc/sysctl.d/99-swappiness.conf

ai3_path=/home/$username/arch_install3.sh
sed '1,/^#part3$/d' arch_install2.sh > $ai3_path
chown $username:$username $ai3_path
chmod +x $ai3_path
su -c $ai3_path -s /bin/sh $username
exit 

#part3
cd /tmp
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg -si --noconfirm

sudo sed -i '17s/.//' /etc/paru.conf
sudo sed -i '35s/.//' /etc/paru.conf
sudo sed -i '38s/.//' /etc/paru.conf

paru -S --noconfirm brave-bin
paru -S --noconfirm zsh-fast-syntax-highlighting
paru -S --noconfirm zramd
paru -S --noconfirm nerd-fonts-hack
paru -S --noconfirm nvim-packer-git
paru -S --noconfirm picom-jonaburg-git 
paru -S --noconfirm peerflix 
paru -S --noconfirm librewolf-bin 
paru -S --noconfirm cava 
paru -S --noconfirm xcursor-breeze 
paru -S --noconfirm ttyper-bin 
paru -S --noconfirm fast

sudo systemctl enable zramd

cd $HOME

git clone https://gitlab.com/voxie12/suckless.git
cd suckless 
cd dwm
sudo make clean install
cd ..
cd dmenu
sudo make clean install
cd ..
cd slstatus
sudo make clean install
cd ..
cd slock
sudo make clean install

cd $HOME
mkdir -p ~/dl ~/vid ~/music ~/doc ~/code ~/pic/wallhaven ~/anime ~/git

yadm clone https://gitlab.com/voxie12/dotfiles.git

mkdir -p ~/.cache/zsh
touch ~/.cache/zsh/history

echo "#######################################"
echo "### Finished! umount -a and reboot ####"
echo "#######################################"

exit
